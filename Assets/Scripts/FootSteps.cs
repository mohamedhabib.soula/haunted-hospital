﻿using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class FootSteps : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] stoneClips;


    private AudioSource audioSource;
  

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        
    }

    private void Step()
    {
        AudioClip clip = GetRandomClip();
        audioSource.PlayOneShot(clip);
    }

    private AudioClip GetRandomClip()
    {


        int index = Random.Range(0, stoneClips.Length);
        return stoneClips[index];
        }
        
    }
