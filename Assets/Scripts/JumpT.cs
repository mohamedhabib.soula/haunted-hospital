﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpT : MonoBehaviour
{
    public AudioSource sc;
    public GameObject player;
    public GameObject camj;
    public GameObject flashing;
    bool isEnabled = true;

    private void Start()
    {
        
    }

    private void OnTriggerEnter()
    {   if (isEnabled)
        {
            sc.Play();
            camj.SetActive(true);
            player.SetActive(false);
            flashing.SetActive(true);
            StartCoroutine(EndJump());
        }
        else
            Debug.Log("NADA");
             

    }
    IEnumerator EndJump()
    {
        yield return new WaitForSeconds(2.03f);
        player.SetActive(true);
        camj.SetActive(false);
        flashing.SetActive(false);
        isEnabled = false;
        //yield return new WaitForSeconds(1f);
        this.enabled = false;
    }
}
